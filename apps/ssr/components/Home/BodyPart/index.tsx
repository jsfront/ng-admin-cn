import CardItem from '@/components/CardItem'
import { Col, Row } from 'antd'

type PageProps = {
  data: any,
}

export default function HomeBodyPart({ data }: PageProps) {
  return (
    <div className="app-body-content">
      <Row gutter={20}>
        {data?.length > 0 && data.map((item: any) => {
          return (
            <Col key={item.id} className="app-admins-col  app-body-col" lg={6} xs={24} sm={8}>
              <CardItem data={item} />
            </Col>
          )
        })}
      </Row>
    </div>
  )
}
