import { Col, Row } from 'antd'
import CardItem from '@/components/CardItem'
import HomeBodyTitle from '../BodyTitle'

type PostPageProps = {
  adminList: any,
}

export default function HomeBodyItem({ adminList }: PostPageProps) {
  const meta = {
    title: 'Admin管理页面',
    url: '/admin',
  }

  return (
    <div className="app-body-item">
      <HomeBodyTitle data={meta} />
      <div className="app-body-content">
        <Row gutter={20}>
          {adminList.length > 0 && adminList.map((item: any) => {
            return (
              <Col key={item.id} className="app-admins-col app-body-col" lg={6} xs={24} sm={8}>
                <CardItem data={item} />
              </Col>
            )
          })}
        </Row>
      </div>
    </div>
  )
}
