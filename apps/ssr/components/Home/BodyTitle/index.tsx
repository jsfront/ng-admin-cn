import Link from 'next/link'
import { OverflowMenuHorizontal, Link as LinkIcon } from '@carbon/icons-react'
import { Tooltip } from 'antd'

type PageProps = {
  data: any,
}

export default function HomeBodyTitle({ data }: PageProps) {
  return (
    <div className="app-body-title flex justify-between svg-rot">
      <h1>
        <Link target="_blank" className="title" title={data.title} href={data.url}>
          <LinkIcon className="mr-2" />
          { data.title }
        </Link>
      </h1>
      <Tooltip title="更多">
        <Link target="_blank" className="more" href={data.url} title="更多">
          <OverflowMenuHorizontal />
        </Link>
      </Tooltip>
    </div>
  )
}
