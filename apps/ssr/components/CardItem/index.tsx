import classNames from 'classnames'
import { Fallback } from '@/config'
import { Card, Space } from 'antd'
import Image from 'next/image'
import Link from 'next/link'
import { useState } from 'react'

type PostPageProps = {
  data: any,
}

export default function CardItem({ data }: PostPageProps) {
  const url = `/post/${data.id}`
  const [hasError, setHasError] = useState(false)
  const src = data.coverUrl ? data.coverUrl : ''

  return (
    <Card className="app-card-item relative">
      {data.isTop > 0 && <i className="absolute app-icon-top" />}
      <div className="title mb-3">
        <h3><Link href={url} title={data.title} target="_blank">{ data.title }</Link></h3>
      </div>
      <Link href={url} target="_blank" className="cover-url next-image">
        <Image onError={() => setHasError(true)} loader={() => (hasError ? Fallback : src)} src={src} alt={data.title} className={classNames('image', { error: hasError })} width={235} height={200} />
      </Link>
      <div className="meta flex justify-between mt-3">
        <div className="author">
          <Space>
            {data.homepage ? <Link className="name" href={data.homepage} title={data.homepage}>{ data.author }</Link> : <span className="name" title={data.homepage}>{ data.author }</span>}
          </Space>
        </div>
        <div className="time">{ data.createTime.slice(0, 11) }</div>
      </div>
    </Card>
  )
}
