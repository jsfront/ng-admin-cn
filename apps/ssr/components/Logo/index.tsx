import config from '@/config'
import Image from 'next/image'
import Link from 'next/link'
import Img from '@/public/images/logo-nav.png'

export default function Logo() {
  return (
    <Link className="rotate-svg" href="/" title={config.title}>
      <Image unoptimized src={Img} alt={config.title} className="mr-3" width={40} height={42} />
      <span className="app-header-title">{config.title}</span>
    </Link>
  )
}
