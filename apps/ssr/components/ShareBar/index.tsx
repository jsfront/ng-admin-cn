import { PlusOutlined } from '@ant-design/icons'
import { Chat } from '@carbon/icons-react'
import classNames from 'classnames'
import { Tooltip } from 'antd'
import Link from 'next/link'

type PageProps = {
  btnUrl: string
  label?: string
  className?: string
}

export default function ShareBar({ btnUrl, label = '提交你的模板', className }: PageProps) {
  return (
    <div className={classNames('app-blog-action', className)}>
      <Tooltip title={label} placement="left">
        <Link href={btnUrl} target="_blank">
          <div className="action-btn">
            <PlusOutlined className="animated" />
          </div>
        </Link>
      </Tooltip>
      <Tooltip title="点击复制" placement="left">
        <div className="action-btn" hidden>
          <Chat className="animated" />
        </div>
      </Tooltip>
    </div>
  )
}
