import Document, { Html, Head, Main, NextScript } from 'next/document'
import CommonScript from '../components/CommonScript'
import AppConfig, { Favicon, BaiduSiteVerification } from '@/config'

const { keywords, description } = AppConfig

class MyDocument extends Document {
  static async getInitialProps(ctx: any) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }

  render() {
    return (
      <Html>
        <Head>
          <link rel="shortcut icon" type="image/x-icon" href={Favicon} />
          <meta name="keywords" content={keywords} />
          <meta name="description" content={description} />
          <meta name="baidu-site-verification" content={BaiduSiteVerification} />
          <CommonScript />
        </Head>
        <body className="light dark:bg-night">
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument
