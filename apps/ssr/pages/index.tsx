import { useEffect, useState } from 'react'
import { LoadingOutlined } from '@ant-design/icons'
import HomeBodyItem from '@/components/Home/BodyItem'
import HomeBodyPart from '@/components/Home/BodyPart'
import HomeBodyTitle from '@/components/Home/BodyTitle'
import Banner from '@/public/images/ng-framework.svg'
import { Space, Spin } from 'antd'
import Layout from '@/layouts/default'
import { fetchArticle } from '@/api'
import AppConfig from '@/config'
import Image from 'next/image'
import Head from 'next/head'
import Link from 'next/link'

const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />

type PostPageProps = {
  adminList: any,
  uiList: any,
}

export default function Index({ adminList, uiList }: PostPageProps) {
  const { title, description, keywords } = AppConfig
  const [loading, setLoading] = useState(true)
  const meta = {
    ui: {
      title: 'UI组件库',
      url: '/ui',
    },
  }

  useEffect(() => {
    setLoading(false)
  }, [adminList])

  return (
    <Layout>
      <Head>
        <title>{`首页 - ${title}`}</title>
        <meta name="description" content={description} />
        <meta name="keywords" content={keywords} />
      </Head>
      <div className="app-home-hero">
        <div className="container flex">
          <div className="main">
            <p className="text">angular管理平台集中地</p>
            <p className="tagline">提交入口</p>
            <div className="actions">
              <Space size={15}>
                <Link className="ant-btn ant-btn-primary ant-btn-block ant-btn-round" href="/post/edit?nav_id=1" target="_blank">管理后台</Link>
                <Link className="ant-btn ant-btn-primary ant-btn-block ant-btn-round ant-btn-dangerous" href="/post/edit?nav_id=2" target="_blank">UI组件库</Link>
              </Space>
            </div>
          </div>
          <div className="image">
            <div className="image-container">
              <Image unoptimized src={Banner} alt="angular管理平台集中地-提交入口" width={250} height={230} />
            </div>
          </div>
        </div>
      </div>
      <div className="app-home-body">
        <div className="container relative">
          {loading && (
            <div className="scope-loading">
              <Spin indicator={antIcon} spinning={loading} />
            </div>
          )}
          <div className="app-home-body-item app-home-admin relative">
            <HomeBodyItem adminList={adminList} />
          </div>
          <div className="app-home-body-item app-hooks-admin mt-2 mb-3">
            <HomeBodyTitle data={meta.ui} />
            <HomeBodyPart data={uiList} />
          </div>
        </div>
      </div>
    </Layout>
  )
}

export async function getServerSideProps() {
  const { rows: adminList } = await fetchArticle({ params: { navId: 1, page: 1, limit: 4 } })
  const { rows: uiList } = await fetchArticle({ params: { navId: 2, page: 1, limit: 4 } })

  return {
    props: {
      adminList,
      uiList,
    },
  }
}
