import { Link as LinkIcon } from '@carbon/icons-react'
import Layout from '@/layouts/default'
import AppConfig from '@/config'
import Image from 'next/image'
import Head from 'next/head'
import { Divider } from 'antd'

const { title } = AppConfig

export default function About() {
  const groupImg = '//pub.idqqimg.com/wpa/images/group.png'

  return (
    <Layout>
      <Head>
        <title>{`关于我们 - ${title}`}</title>
      </Head>
      <div className="app-page-about shadow bg-white container mx-auto my-6 p-6 white">
        <div className="app-about-title text-xl mb-4 text-center">
          <h1>关于我们</h1>
        </div>
        <Divider />
        <div className="app-about-body flex content-center pt-15 justify-center">
          <div className="about-main mr-1">
            <div className="app-main-title text-xl text-gray-600">
              <h1>Hi，我们是一群一直关注Angular的小伙伴，</h1>
            </div>
            <div className="app-about-content pt-2">
              <p>加油，我们一直在路上。</p>
              <h2>前台仓库</h2>
              <p>
                <a target="_blank" rel="noreferrer" className="rotate-svg" href="https://gitee.com/jsfront/ng-admin-cn">
                  <LinkIcon className="mr-2" />https://gitee.com/jsfront/ng-admin-cn
                </a>
              </p>
              <h2>API仓库</h2>
              <p>
                <a target="_blank" rel="noreferrer" className="rotate-svg" href="https://gitee.com/jsfront/nest-admin-api">
                  <LinkIcon className="mr-2" />https://gitee.com/jsfront/nest-admin-api
                </a>
              </p>
            </div>
          </div>
          <div className="contact-main">
            <div className="app-main-title text-xl text-gray-600">
              <h1>可以通过以下方式找到我们：</h1>
            </div>
            <div className="app-about-content pt-2">
              <h2>Angular反馈Q群：238251402</h2>
              <p>
                <a target="_blank" href="https://qm.qq.com/cgi-bin/qm/qr?k=ybf3wCnhfLucWMOPLVrW3tsBqRy-TngS&jump_from=webapi&authKey=VrkypVLqirLnW3NaRodGOSChSMVQwEVX7R9TLY71SiOaBCQCVMqRtvo8kPwAH7mK" rel="noreferrer">
                  <Image loader={() => groupImg} unoptimized src={groupImg} alt="React" width={90} height={22} />
                </a>
              </p>
              <h2>Node反馈Q群：422910907</h2>
              <p>
                <a target="_blank" href="https://qm.qq.com/cgi-bin/qm/qr?k=-Jr4gqZRj872N5JeKbJaqIrvrlu3maSq&jump_from=webapi&authKey=ZHUuc+ivFOkGyIqx1Mib8wK1UQudDakNhpBwWuDYFUowIFV+HBbJZXDukdw0i1wO" rel="noreferrer">
                  <Image loader={() => groupImg} unoptimized src={groupImg} alt="React" width={90} height={22} />
                </a>
              </p>
              <p>微信群满200人，加Q群后邀请加入。</p>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  )
}
