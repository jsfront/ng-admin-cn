import { fetchRequest } from '@/utils/request'

// 文章列表
export const fetchArticle = (params?: any) => fetchRequest({ api: 'article', ...params })
// 新增文章
export const fetchArticleAdd = (params?: any) => fetchRequest({ api: 'article/add', ...params })
// 后端语言列表
export const fetchBackend = (params?: any) => fetchRequest({ api: 'backend', ...params })
// 标签列表
export const fetchTags = (params?: any) => fetchRequest({ api: 'tags', ...params })
// 文章详情
export const fetchArticleDetail = (params?: any) => fetchRequest({ api: 'article/detail', ...params })
// 文章分类列表
export const fetchArticleCategory = (params?: any) => fetchRequest({ api: 'article/category/list', ...params })
// 标签列表
