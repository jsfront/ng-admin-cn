import { Domain } from '@/config'
import Link from 'next/link'
import { Link as LinkIcon } from '@carbon/icons-react'
import { useSetState } from 'ahooks'
import { Space } from 'antd'

export default function Footer() {
  const [state] = useSetState({
    // 列表
    links: [
      { name: 'vue-admin.cn', title: 'vue管理平台', url: 'http://vue-admin.cn' },
      { name: 'react-admin.cn', title: 'react管理平台', url: 'http://react-admin.cn' },
    ],
    year: new Date().getFullYear(),
  })

  const docsUrl = `https://${Domain}/doc`

  return (
    <div className="app-footer">
      <div className="container flex items-center justify-between">
        <div className="app-footer-logo svg-rot">
          <Space size={20}>
            {state.links.map((link) => {
              return (
                <Link key={link.name} href={link.url} title={link.title} target="_blank">
                  <Space size={8}>
                    <LinkIcon />
                    <span>{link.name} </span>
                  </Space>
                </Link>
              )
            })}
          </Space>
        </div>
        <div className="nav text-blue-600">
          <Space size={20}>
            <Link href="/">首页</Link>
            <Link href="/help/about">关于</Link>
            <Link href={docsUrl} target="_blank">帮助中心</Link>
          </Space>
        </div>
        <div className="copyright text-xs font-sans">
          <span className="mr-3"><Link href="/" className="text-blue-600">{Domain}</Link></span>
          <span> © {state.year}. All rights reserved.</span>
        </div>
      </div>
    </div>
  )
}
