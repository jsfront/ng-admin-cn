/* eslint-disable max-len */
import pkg from '../package.json'
import { Empty } from 'antd'

// 存储key
export const StoreKey = 'ng-admin-cn'
export const TokenKey = `${StoreKey}-Token`
export const CITY_CODE = process.env.NEXT_PUBLIC_CITY_CODE || ''

// 分页配置
export const PageConfig: any = {
  base: { current: 1, page: 1, limit: 8 },
  options: { showQuickJumper: true, current: 1, page: 1, showSizeChanger: true, pageSizeOptions: ['5', '10', '20', '30', '50', '100'] },
}

// 基础配置
export default {
  name: pkg.name,
  version: pkg.version,
  title: 'Ng-Admin',
  author: 'Admin',
  keywords: 'angular交流群 - 238251402',
  description: 'angular前端,angular后台管理模板,angular管理模板',
}

export const ROUTES_LINKS = [
  { title: '首页', path: '/' },
  { title: 'Admin', path: '/admin' },
  { title: 'UI组件库', path: '/ui' },
  // { title: 'Hooks', path: '/hooks' },
  { title: '关于我们', path: '/help/about/', isBlank: false },
  // { title: '帮助中心', path: '/docs', isBlank: true },
]

interface ICodeMessage {
  [propName: number]: string
}

export const CodeMessage: ICodeMessage = {
  200: '服务器成功返回请求的数据',
  201: '新建或修改数据成功',
  202: '一个请求已经进入后台排队（异步任务）',
  204: '删除数据成功',
  400: '发出的请求有错误，服务器没有进行新建或修改数据的操作',
  401: '用户没有权限（令牌、用户名、密码错误）',
  403: '用户得到授权，但是访问是被禁止的',
  404: '发出的请求针对的是不存在的记录，服务器没有进行操作',
  405: '请求方法不被允许',
  406: '请求的格式不可得',
  410: '请求的资源被永久删除，且不会再得到的',
  422: '当创建一个对象时，发生一个验证错误',
  500: '服务器发生错误，请检查服务器',
  502: '网关错误',
  503: '服务不可用，服务器暂时过载或维护',
  504: '网关超时',
}
// 域名
export const Domain = 'ng-admin.cn'
// APPID
export const App_Id = 3
// 标签颜色
export const TagColorList = ['red', 'orangered', 'orange', 'deeppink', 'lightcoral', 'green', 'cornflowerblue', 'blue', 'darkslategrey', 'purple', 'pinkpurple', 'magenta', 'gray', 'darkolivegreen', 'lightseagreen']
// 默认图片
export const Fallback = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg=='
export const Favicon = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAGAElEQVR4AdVWA5RzXRJ8Y9u2kjyFv23btm1+tm3bHH62rbFtM+nte+5kR9F6/zqnhkmqum/dfs38JXFWJjKnpbzNeV4RkiYTfKdHxlqtjE/6z4pmJMqYtTEJVlc4RWCOoHqqUFQvvC6qMlM58exuVpywixXu3SrlvEcFh1stjEn494guj45nnvPyZi6wci8UvTdfVE8ulGsuFck1HcVyDdwQVZDCibCbsgV5cjcrjN7JCndukXKen/v6MjOjYv4x0UVRcQy89DZzhhVdsgTVTSj6J4oeR9EWJOhJDaipgaFsQh5DM8PRzK2bJaz7a26ezOzoOMOisyJjmCrFzcwJmeCQKSjZPFH9dYFcnYFCdUgwSBQv/fgzOPDYk7BLxlNhw2xAHkIzv+2U8ZpNEtaVQaRLWObvgAefYG7wSk2BqN6K1ZajgA4JpsSLb78bOk6dhqx58wHPn4qZZx1y3w4Zfzc8+Fg/Ay3AnGXl7+qFzZJXQvWX3wB0dkJzXh7se+Bhc13oT91aiew9AGD6w2p3ouxZrL7DEgPFmtugNT0DgECng6vjJ1psYBcnts+MTXgaNa2ZfrBeGhN/Fwau3pLqK956D7QNjaBHw9WrkHHXvbDbgqPAUNYOi4y+Y7ABq3HhUXyuqC4ya0BxEzSv30iL12qBdEDX0wMX/xgGu6ScWQPbWSHvk+BQKdEcYOCTgOAovHaXTIoLKih/7iXoqaiglR84CM2ZmUBQe+YMpN16h9kubJXxZx7z9g0bYuAONw//a7xyv7n0N8xbSKtvbYX89z+C6+MmAIEWA3n22+/NdmGTlEuOdnT0oQYGwv0ip1hv8t4//Dh05eYBQcfxk5B58+2w//6HoCk7BwgqDx2CVPXNJruwVsIuRi0XxgCcTrPiDFPhqxs7AciZA5593YjRcINTQDKm//q06UDQg1059fGnJruwPFE6ErUcGAOwOyLlfzFWfcld90PHhYtA0JWTC6UPPALXWRGSJSzsfeAhIPOAoDQlFVIUamMGtPPjkz5GLVvGAGzSkth3cRb0GKq+5sdfQNfVBQRte/dBJV7FwjffheOvvwlHX3uDCFNzjY1w7M23DXYBZ0DblJh4OgMMwHpjXNLj+AxoHTJ2b74D2g4dBj10GDgSQi2yu5fajg7Qo2jTZkgWFIZmQM2fkdG3GTNgNT8q7hZ8EFUPrr7qw09B29JCP53c/Z4e+h2p0xP/Rr4TdFRXw+EXXhoyHXEG5H4cHCohWgYNDAuNkOSIqtwBBlS3QMuOnbTytjaonz4Lqr/5AWq++wlKv/sRzn3zHZxFnvnqG7g2YRJ0NTUBQe7yFYZmwKmHvH1CjRp41dc/LFNQnek/eCpeeQN6amppZWfPQfGtd0Ehpp905jonh2QpB7sIMYwkfBX799OclJbCgSeeGtCFjVJuV6i9g7dRA1InZ5+rvDK1fwealq8EPeomTCbCRhcSYuTc9z+CtjesmbPnDHhU4wxYSGeAcbhe4OTL9dWXPfUcdJeUAEF3aRmUPfkc+bvxjQjFMu64G+ovXwYEXs182PfQo7QLdAYMRw17xgQccCsaV9xv7jfMmoucA9Xf/wRFypvNr2Ro4hRuStnzFpAOwMEnn4bd1ED3nPjE9+kMMA774aERtx6TCVvwNjQVi/QWIGnlFu6EWDHNBRINkfCVzI5LnPqot2+COQM2SA8MStKkiOiPcDE9hGtaRzEVtdgAYTK9drXzE5LWvOAX8KStlVUEPX8aQHMmXJGBnLOLYnF0/E+XOMVFMiGLLTCQTAdOy7JEacpHwSGvu9nYkKp9kU50AFkGK6Qd0h0Zcr+H1+2b4iUTrvPKPBTXGTKQTEdt5xoJe/zH8MjPQxwcOHyvP60ai/onYdWbWk9kxGu+/g+lJrKLcHGp0BtI7X3IbJByV0dFxfwhc3FV42uDkG5IW/oZ/zqskY5Ib0dr69hvg0KfPyzlN18WlA3bZHzhlNj4qXd6et2F/w9FeiDt/nlh8/lwRvqF2NtLfg4Jf+klv4BH0F0k/s0L6fBPCf8Tx2LbG1QfImwmYCbxN17E6iOijFB2AAAAAElFTkSuQmCC'
// tinymce key
export const TinyKey = 'i9loif5tptrktatd5nsxz66i1iqnijfpotc1lqs9mf24za7q'
export const EmptyStatus = <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
// 百度统计
export const baiduGA = 'https://hm.baidu.com/hm.js?a56dac4a658f0905da8cf81de9acb78f'
// 百度资源平台验证
export const BaiduSiteVerification = "codeva-IKqKsOcqDr"

